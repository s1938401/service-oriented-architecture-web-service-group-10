# Braxburgers Application

## Run the application
To run the application you need the following dependencies:
- Minikube
- kubectl

Navigate to the k8s-config directory (in project root folder, do `cd microservices/k8s-config/`) and execute the following commands:
- `minikube start`
- `minikube addons enable ingress`
- `kubectl create -f restaurant-deployment.yml`
- `kubectl create -f inventory-deployment.yml`
- `kubectl create -f hrm-deployment.yml`
- `kubectl create -f worker-deployment.yml`
- `kubectl create -f message-queue-deployment.yml`
- `kubectl create -f ingress.yml`

And then in other terminal windows:
- `minikube tunnel`
- (opt) `minikube dashboard`

The Ingress works with the host name `www.braxburgers.com`. We have to map this locally to acces the application via this host.
Minikube will automatically give the ingress an IP address. We can map this to the host. 
- Look up the ww.braxburgers.com IP address (in the dashboard under Services>Ingress>Endpoints or via `kubectl get ingress -n default`). If none is displayed the service is still booting.
- Add the mapping from IP to host to the `/etc/hosts` file. 
  - `sudo nano /etc/hosts`
  - Add <Ingress IP>   www.braxburgers.com (for example under localhost mapping)
  - Example: `192.168.1.2     www.braxburgers.com`
  - Save the file 

Now `www.braxburgers.com` is accessible and exposes the frontend for customers while `www.braxburgers.com/admin` serves the admin page.

You can also determine the external IPs (found in dashboard under Service>Services>External Endpoints) of the restaurant-service and access the API documentation for the services at `http://<EXTERNAL-IP>/swagger-ui.html`
Via the deployment with Kubernetes, the application can be easily scaled up and down per service.

## Tests
The tests were made for development without Kubernetes and are disabled in this repo. This was due to the restaurant-service tests requiring the inventory-service to be able to run, which is not possible when running `mvn clean package` (unless using `-Dmaven.test.skip`).
