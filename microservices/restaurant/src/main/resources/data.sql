-- equipment (id bigint not null, status varchar(255), primary key (id))
-- equipment_product_ids (equipment_id bigint not null, product_ids bigint)
-- kitchen (id bigint not null, status varchar(255), primary key (id))

INSERT INTO kitchen (id, status) VALUES
    (1, 'OPEN');

INSERT INTO equipment (id, status) VALUES
    (1, 'OK'),
    (2, 'OK'),
    (3, 'BROKEN');

INSERT INTO equipment_product_ids (equipment_id, product_ids) VALUES
    (1, 1),
    (1, 2),
    (2, 1),
    (3, 1);