let order = [];
let nameIdMap = new Map();
let list = document.getElementById("current-order-list");
let placeOrderButton = document.getElementById("place-order-button");
placeOrderButton.disabled = true;
var ws;
let orderId = "?";
let hasStarted = false;
let pickUpOrderButton = document.getElementById("pickup-order-button");
let statusText = document.getElementById("status-text");

function addProductToOrder(name, id) {
    // Add the name to the order list.
    order.push(name);
    if (!nameIdMap.has(name)) {
        nameIdMap.set(name, id);
    }
    console.log("A "+name+" > "+order);
    // let orderButton = document.getElementById("order-" + name);
    // orderButton.disabled = true;

    // Undisable place order button
    placeOrderButton.disabled = false;

    // Add HTML list item to the current order list.
    let li = document.createElement("li");
    li.classList.add("list-group-item");
    li.innerText = name;
    li.id = "li-tag-" + name;

    let removeButton = document.createElement("button");
    removeButton.innerText = "Remove"
    removeButton.className = "btn btn-danger btn-sm mx-0 ml-3 px-1 py-0"
    removeButton.onclick = function() {removeProductToOrder(name)}
    li.appendChild(removeButton);

    list.appendChild(li);
}

function removeProductToOrder(name) {
    order.splice(order.indexOf(name),1);
    let orderButton = document.getElementById("order-" + name);
    orderButton.disabled = false;
    console.log("R "+name+" > "+order);
    // Disable place order button if not items left.
    if (order.length === 0) {
        placeOrderButton.disabled = true;
    }

    let liTag = document.getElementById("li-tag-" + name);
    list.removeChild(liTag);
}

async function placeOrder(url) {
    // Display spinner
    let prepareSpinner = document.getElementById("preparing-order-spinner");
    prepareSpinner.hidden = false;
    statusText.textContent = "Fetching Order..."
    placeOrderButton.hidden = true;
    document.getElementById("modify-order-button").hidden = false;
    // Get the product ids.
    let idList = [];
    order.forEach(name => idList.push(nameIdMap.get(name)));
    let idListString = idList.join(",");
    console.log(url)
    document.getElementById("context-text").innerText = "As long as your order is in the queue, you can edit your order.";
    // Setup websocket
    connect(url,"O:"+idListString)
}

async function modifyOrder() {
    let idList = [];
    order.forEach(name => idList.push(nameIdMap.get(name)));
    let idListString = idList.join(",");
    ws.send("M:"+orderId+":"+idListString)
}

async function pickupOrder(){
    document.getElementById("context-text").innerText = "Your order has been completed.";
    document.getElementById("pickup-order-button").hidden = true;
    document.getElementById("success-placement-alert").hidden = true;
    document.getElementById("finished-alert").hidden = false;
    await ws.send("P:"+orderId);
    ws.close();
}

function connect(url,data) {
    let statusText = document.getElementById("status-text");
    statusText.textContent = "Setting up Websocket..."
    ws = new WebSocket(url+"/user");
    ws.onmessage = function (data){
        receiveMessage(data.data)
    }
    statusText.textContent = "Sending Order..."
    ws.onopen = () => ws.send(data);
}

function receiveMessage(message){
    console.log(message);
    const parts = message.split(':');
    switch (parts[0]) {
        case "Q":
            orderId = parts[1];
            document.getElementById("id-text").textContent = "ID-number: "+orderId;
            statusText.textContent = "Queued Order...";
            placeOrderButton.hidden = true;
            // let allAddOrderButtons = document.getElementsByClassName("add-order-button");
            // for (let i = 0; i < allAddOrderButtons.length; i++) {
            //     allAddOrderButtons[i].disabled = true;
            // }
            let successAlert = document.getElementById("success-placement-alert");
            successAlert.hidden = false;
            let currentOrderTitle = document.getElementById("current-order-title");
            currentOrderTitle.innerText = "Your order:";
            if (!hasStarted) break;
        case "S":
            hasStarted = true;
            document.getElementById("modify-order-button").hidden = true;
            let allRemoveButtons = document.getElementsByClassName("btn btn-danger btn-sm mx-0 ml-3 px-1 py-0")
            for (let i=0;i<allRemoveButtons.length;i++){
                allRemoveButtons[i].hidden = true;
            }
            document.getElementById("context-text").innerText = "Your order is now being made in the restaurant!";
            document.getElementById("menu-div").hidden = true;
            statusText.textContent = "Preparing Order...";
            break;
        case "R":
            statusText.textContent = "Packing Order...";
            break;
        case "P":
            document.getElementById("context-text").innerText = "Please pick up your order, enjoy!";
            let prepareSpinner = document.getElementById("preparing-order-spinner");
            prepareSpinner.hidden = true;
            pickUpOrderButton.hidden = false;
            break;
        default:
            console.log(message);
    }
    //     , _ => {
    //     let failureAlert = document.getElementById("fail-placement-alert");
    //     failureAlert.hidden = false;
    // }
}
