package com.g10.microservices.restaurant.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.g10.microservices.restaurant.entity.*;
import com.g10.microservices.restaurant.repository.*;
import com.g10.microservices.restaurant.websocket.WebsocketHandler;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import static com.g10.microservices.restaurant.constants.*;

@RestController
public class RestaurantController {
    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    KitchenRepository kitchenRepository;

    @Autowired
    EquipmentRepository equipmentRepository;

    @Autowired
    WebsocketHandler websocketHandler;

    @ApiOperation(
            value = "Get all available products",
            notes = "Get a list of all product in the database with a stock of more than 0. This endpoint uses the inventory service to get the products."
    )
    @ApiResponses({
            @ApiResponse(code = 503, message = "The inventory service is unavailable")
    })
    @GetMapping("/product")
    @ResponseBody
    public ResponseEntity<List<Product>> getAllProducts(){
        return getAvailableProducts();
    }

    public ResponseEntity<List<Product>> getAvailableProducts() {
        String endpoint = "http://" + INVENTORY_SERVICE_ID + "/admin/product/";
        try {
            ResponseEntity<Product[]> productResponse = this.restTemplate.exchange(endpoint,
                    HttpMethod.GET, null, Product[].class);
            if (productResponse.getBody() != null) {
                Stream<Product> availableProducts = Arrays.stream(productResponse.getBody()).filter(product -> product.getStock() > 0);
                return ResponseEntity.ok(availableProducts.collect(Collectors.toList()));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(503).build();
        }

        return ResponseEntity.ok().build();
    }

    @ApiOperation(
            value = "Get the kitchen status",
            notes = "Get the current status of the kitchen, OPEN or CLOSED."
    )
    @GetMapping("/kitchen/status")
    @ResponseBody
    public KitchenStatus getKitchenStatus(){
        return kitchenRepository.findAll().get(0).getStatus();
    }

    @ApiOperation(
            value = "Get an order by id",
            notes = "Provide an id and get information about that specific order. Returns not-found error if the id is not present."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable Long id){
        Order order = orderRepository.getById(id);
        if (order != null) {
            return ResponseEntity.ok(order);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Create an order",
            notes = "Provide an order object and create a new order. The order also gets sent to a message queue for processing it."
    )
    @PostMapping("/order")
    @ResponseBody
    public Order createOrder(@RequestBody Order newOrder){
        newOrder.setStatus(OrderStatus.TODO);
        Order createdOrder = orderRepository.save(newOrder);
        jmsTemplate.convertAndSend(ORDER_QUEUE, createdOrder.getId());
        return createdOrder;
    }

    public Order createOrder(String products,String sessionId){
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts(products);
        order.setSessionId(sessionId);

        Order createdOrder = orderRepository.save(order);
        Long id = createdOrder.getId();
        jmsTemplate.convertAndSend(ORDER_QUEUE, id);
        return createdOrder;
    }

    // For testing purposes only.
    @PostMapping("/order/create/testorder")
    @ResponseBody
    public Order createTestOrder(@RequestBody Order order){
        order.setStatus(OrderStatus.TODO);
        Order createdOrder = orderRepository.save(order);
        Long id = createdOrder.getId();
        jmsTemplate.convertAndSend(ORDER_QUEUE, id);
        return createdOrder;
    }

    // For testing purposes only.
    @PostMapping("/order/create/test")
    @ResponseBody
    public Order createTestOrder(){
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        String products = "1,2,3,3,3,3";
        order.setProducts(products);
        Order createdOrder = orderRepository.save(order);
        Long id = createdOrder.getId();
        jmsTemplate.convertAndSend(ORDER_QUEUE, id);
        return createdOrder;
    }

    // For testing purposes only.
    @PostMapping("/order/create/test-batch")
    @ResponseBody
    public List<Order> createTestOrderBatch(){
        List<Order> listCreatedOrders = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Order order = new Order();
            order.setStatus(OrderStatus.TODO);
            String products = "1,2,3,4,5";
            order.setProducts(products);
            Order createdOrder = orderRepository.save(order);
            Long id = createdOrder.getId();
            jmsTemplate.convertAndSend(ORDER_QUEUE, id);
        }
        return listCreatedOrders;
    }

    @ApiOperation(
            value = "Update an order",
            notes = "Provide an id and an order object that will update the current order with the that id."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order has already gone through tp the kitchen and can no longer be updated."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @PutMapping("/order/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable Long id, @RequestBody Order newOrder) {
        Order oldOrder = orderRepository.getById(id);
        if (oldOrder != null) {
            if (oldOrder.getStatus() != OrderStatus.TODO){
                return ResponseEntity.badRequest().build();
            }
            newOrder.setId(oldOrder.getId());
            return ResponseEntity.ok(orderRepository.save(newOrder));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Update an order using string of products",
            notes = "Provide an id and string fo products that will update the current order with the that id."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order has already gone through tp the kitchen and can no longer be updated."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @PutMapping("/order/{id}/products")
    public ResponseEntity<Order> updateOrderWithProducts(@PathVariable Long id, @RequestBody String products) {
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.TODO){
                return ResponseEntity.badRequest().build();
            }
            order.setProducts(products);
            Order updatedOrder = orderRepository.save(order);
            return ResponseEntity.ok(updatedOrder);
        }
        return ResponseEntity.notFound().build();
    }

    // For testing purposes only.
    @PutMapping("/order/{id}/test")
    public ResponseEntity<Order> testUpdateOrder(@PathVariable Long id, @RequestBody Order newOrder) {
        Order oldOrder = orderRepository.getById(id);
        if (oldOrder != null) {
            newOrder.setId(oldOrder.getId());
            return ResponseEntity.ok(orderRepository.save(newOrder));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get the kitchen",
            notes = "Get the current kitchen object."
    )
    @GetMapping("/kitchen")
    @ResponseBody
    public Kitchen getKitchen() {
        return kitchenRepository.findAll().get(0);
    }

    @ApiOperation(
            value = "Get all orders",
            notes = "Get a list with all orders in the database."
    )
    @GetMapping("/order")
    @ResponseBody
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @ApiOperation(
            value = "Get all kitchen equipment",
            notes = "Get a list with all equipment in the database."
    )
    @GetMapping("/kitchen/equipment")
    @ResponseBody
    public List<Equipment> getAllEquipment() {
        return equipmentRepository.findAll();
    }

    @ApiOperation(
            value = "Get kitchen equipment by id",
            notes = "Provide an id and get that specific equipment if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The equipment with the specified id does not exists in the database"),
    })
    @GetMapping("/kitchen/equipment/{id}")
    public ResponseEntity<Equipment> getEquipment(@PathVariable Long id) {
        Equipment equipment = equipmentRepository.getById(id);
        if (equipment != null) {
            return ResponseEntity.ok(equipment);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get the worker schedule",
            notes = "Get the worker schedule from the HRM service."
    )
    @ApiResponses({
            @ApiResponse(code = 503, message = "The HRM service is unavailable")
    })
    @GetMapping("/admin/schedule")
    public ResponseEntity<String> getSchedule() {
        String endpoint = "http://" + HRM_SERVICE_ID + "/admin/schedule/";
        try {
            ResponseEntity<String> scheduleResponse = this.restTemplate.exchange(endpoint,
                    HttpMethod.GET, null, String.class);
            if (scheduleResponse.getBody() != null) {
                return ResponseEntity.ok(scheduleResponse.getBody());
            }
        } catch (Exception e) {
            return ResponseEntity.status(503).build();
        }

        return ResponseEntity.ok().build();
    }

    @ApiOperation(
            value = "Mark order as complete, ready to be picked up",
            notes = "Provide an id and mark the order with that id as complete by setting its status."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is not of status READY."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}/complete")
    public ResponseEntity<Order> completeOrder(@PathVariable Long id) throws IOException {
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.READY){
                return ResponseEntity.badRequest().build();
            }
            order.setStatus(OrderStatus.PACKED);
            websocketHandler.sendMessageToSessionId(order.getSessionId(),"P");
            return ResponseEntity.ok(orderRepository.save(order));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Mark order as ready",
            notes = "Provide an id and mark the order with that id as ready by setting its status."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is not of status START."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}/ready")
    public ResponseEntity<Order> readyOrder(@PathVariable Long id) throws IOException {
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.START){
                return ResponseEntity.badRequest().build();
            }
            order.setStatus(OrderStatus.READY);
            websocketHandler.sendMessageToSessionId(order.getSessionId(),"R");
            return ResponseEntity.ok(orderRepository.save(order));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Mark order as start",
            notes = "Provide an id and mark the order with that id as start by setting its status."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is not of status TODO."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}/start")
    public ResponseEntity<Order> startOrder(@PathVariable Long id) throws IOException {
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.TODO){
                return ResponseEntity.badRequest().build();
            }
            System.out.println("A worker has started on order: "+id.toString());
            websocketHandler.sendMessageToSessionId(order.getSessionId(),"S");
            order.setStatus(OrderStatus.START);
            return ResponseEntity.ok(orderRepository.save(order));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is not of status TODO."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}/pickup")
    public ResponseEntity<Order> pickupOrder(@PathVariable Long id) throws IOException {
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (!(order.getStatus() == OrderStatus.PACKED || order.getStatus() == OrderStatus.CANCELLED)){
                return ResponseEntity.badRequest().build();
            }
            System.out.println("Order "+id.toString()+" has been picked up!");
            websocketHandler.sendMessageToSessionId(order.getSessionId(),"F");
            order.setStatus(OrderStatus.PICKEDUP);
            return ResponseEntity.ok(orderRepository.save(order));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Delete an order",
            notes = "Provide an id and delete the order with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is already being made and can not be deleted."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @DeleteMapping("/order/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteOrder(@PathVariable Long id){
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.TODO){
                return ResponseEntity.badRequest().build();
            }
            orderRepository.delete(order);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Decrement the stock of a product",
            notes = "Provide an id and decrement the stock of the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "There are no products in the database", response = List.class),
            @ApiResponse(code = 400, message = "Bad request, the stock of the product cannot be a negative value"),
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
            @ApiResponse(code = 503, message = "The inventory service is unavailable")
    })
    @GetMapping("/product/{id}/decr")
    @ResponseBody
    public ResponseEntity<Product> decrementProduct(@PathVariable Long id){
        String endpoint = "http://" + INVENTORY_SERVICE_ID + "/admin/product/" + id + "/decr/";
        try {
            ResponseEntity<Product> decrementResponse = this.restTemplate.exchange(endpoint,
                    HttpMethod.GET, null, Product.class);
            if (decrementResponse.getBody() != null) {
                // The inventory service can respond with 200, 400 or 404 status codes.
                switch(decrementResponse.getStatusCode()) {
                    case BAD_REQUEST:
                        return ResponseEntity.badRequest().build();
                    case NOT_FOUND:
                        return ResponseEntity.notFound().build();
                    default: // OK
                        return ResponseEntity.ok(decrementResponse.getBody());
                }
            }
        } catch (Exception e) {
            return ResponseEntity.status(503).build();
        }

        return ResponseEntity.noContent().build();
    }

    @ApiOperation(
            value = "Fix a piece of kitchen equipment",
            notes = "Provide an id and fix the equipment with that id if it exists by setting its status."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The equipment with the specified id does not exists in the database"),
    })
    @GetMapping("/kitchen/equipment/{id}/fix")
    public ResponseEntity<Equipment> fixEquipment(@PathVariable Long id){
        Equipment equipment = equipmentRepository.getById(id);
        if (equipment != null) {
            equipment.setStatus(EquipmentStatus.OK);
            return ResponseEntity.ok(equipmentRepository.save(equipment));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Create a piece of kitchen equipment",
            notes = "Provide an equipment object and create the new equipment."
    )
    @PostMapping("/kitchen/equipment")
    @ResponseBody
    public Equipment createEquipment(@RequestBody Equipment newEquipment){
        // Set status of new equipment to OK.
        newEquipment.setStatus(EquipmentStatus.OK);
        return equipmentRepository.save(newEquipment);
    }

    @ApiOperation(
            value = "Delete a piece of kitchen equipment",
            notes = "Provide an id and delete the equipment with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The equipment with the specified id does not exists in the database"),
    })
    @DeleteMapping("/kitchen/equipment/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteEquipment(@PathVariable Long id){
        Equipment equipment = equipmentRepository.getById(id);
        if (equipment != null) {
            equipmentRepository.delete(equipment);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
