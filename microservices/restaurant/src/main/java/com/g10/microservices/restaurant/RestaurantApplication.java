package com.g10.microservices.restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.jms.ConnectionFactory;

@SpringBootApplication
@EnableJms
@EnableSwagger2
public class RestaurantApplication {
	@Bean
	public JmsListenerContainerFactory<?> jmsTopicContainerFactory(
			ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer )
	{
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		configurer.configure( factory, connectionFactory );
		factory.setPubSubDomain( true );
		factory.setErrorHandler(t -> {
			System.out.println("DefaultJmsListenerContainerFactory -> "+ t);
		});
		return factory;
	}

	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.g10.microservices.restaurant"))
				.build()
				.apiInfo(apiDetails())
				.useDefaultResponseMessages(false);
	}

	private ApiInfo apiDetails() {
		String description = "This is documentation for the restaurant service of the Brax Burgers application. "
				+ "This is a project for the course Service-Oriented Architectures with Webservices. "
				+ "Made by students from University of Twente: Max Lievense and Bram Dekker.";

		return new ApiInfoBuilder()
				.title("Restaurant Service API Documentation")
				.description(description)
				.version("1.0.0")
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(RestaurantApplication.class, args);
	}

}
