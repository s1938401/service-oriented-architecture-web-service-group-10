package com.g10.microservices.restaurant.controller;

import com.g10.microservices.restaurant.entity.Order;
import com.g10.microservices.restaurant.entity.OrderStatus;
import com.g10.microservices.restaurant.entity.Product;
import com.g10.microservices.restaurant.repository.EquipmentRepository;
import com.g10.microservices.restaurant.repository.OrderRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.g10.microservices.restaurant.constants.*;

@Controller
public class FrontendController {
    @Autowired
    EquipmentRepository equipmentRepository;

    @Autowired
    RestaurantController restaurantController;

    @Autowired
    OrderRepository orderRepository;

    @ApiOperation(
            value = "Frontend for the restaurant service",
            notes = "On this page, customers can view the menu and make orders. They can then also pick-up their order."
    )
    @GetMapping( "/")
    public String serveFrontend(Model model) {
        // Use own controller logic to get all available products.
        List<Product> availableProducts = restaurantController.getAvailableProducts().getBody();

        model.addAttribute("productList", availableProducts);
        model.addAttribute("url", "ws://"+DOMAIN);
        return "index";
    }

    @ApiOperation(
            value = "Frontend for the restaurant service",
            notes = "On this page, customers can view the menu and make orders. They can then also pick-up their order."
    )
    @GetMapping( "/admin")
    public String serveAdminFrontend(Model model) {
        model.addAttribute("url_product", "http://"+DOMAIN+"/admin/product");
        model.addAttribute("url_order", "http://"+DOMAIN+"/order");
        return "admin";
    }

    public String processIncomingMessage(String id, String message) throws IOException {
        String[] parts = message.split(":");
        System.out.println(id + " || " + Arrays.toString(parts));
        switch(parts[0]){
            case "O":
                Order order = restaurantController.createOrder(parts[1],id);
                return "Q:"+order.getId().toString();
            case "M":
                restaurantController.updateOrderWithProducts(Long.valueOf(parts[1]),parts[2]);
            case "P":
                restaurantController.pickupOrder(Long.valueOf(parts[1]));
            default:
                System.out.println(message);
                return "ERROR";
        }
    }

    public void removeSessionOrders(String id) {
        List<Order> allOrders = orderRepository.findAll();
        System.out.println("REMOVING ALL ORDERS OF SESSION: "+id);
        for (Order order: allOrders){
            if (order.getSessionId().equals(id)) {
                if (order.getStatus() == OrderStatus.PACKED) {
                    order.setStatus(OrderStatus.PICKEDUP);
                    continue;
                } else if (order.getStatus() == OrderStatus.PICKEDUP) {
                    continue;
                }
                System.out.println("DELETED ORDER "+order.getId());
                order.setStatus(OrderStatus.CANCELLED);
                orderRepository.save(order);
            }
        }
    }
}
