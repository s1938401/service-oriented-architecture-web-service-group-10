package com.g10.microservices.restaurant.controller;

import com.g10.microservices.restaurant.entity.Order;
import com.g10.microservices.restaurant.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import static com.g10.microservices.restaurant.constants.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class PackQueueController {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    private RestaurantController restaurantController;

    @JmsListener(destination = PACK_QUEUE)
    public void onMessage(String message) throws InterruptedException, IOException {
        System.out.println(PACK_DEBUG + "Received message { " + message +" }");
        Long id = Long.parseLong(message);
        ResponseEntity<Order> orderEntity = restaurantController.readyOrder(id);
        if (orderEntity != null && orderEntity.hasBody()){
            packOrder(id);
        } else {
            System.out.println(PACK_DEBUG + " !! ERROR !! - No body in { " + message +" }");
        }
    }

    public void packOrder(Long id) throws InterruptedException, IOException {
        TimeUnit.MILLISECONDS.sleep(PACK_TIME_MILLI);
        System.out.println(PACK_DEBUG + "order " + id.toString()+ " is ready to be picked up!");
        restaurantController.completeOrder(id);
    }
}
