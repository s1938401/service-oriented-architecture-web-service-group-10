 package com.g10.microservices.restaurant.entity;

import java.util.List;
import javax.persistence.*;

@Entity
public class Kitchen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private KitchenStatus status;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public KitchenStatus getStatus() {
        return status;
    }
}