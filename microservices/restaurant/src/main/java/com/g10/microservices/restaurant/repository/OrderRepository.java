package com.g10.microservices.restaurant.repository;

import com.g10.microservices.restaurant.entity.Equipment;
import com.g10.microservices.restaurant.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findAll();
    Order getById(Long id);
}