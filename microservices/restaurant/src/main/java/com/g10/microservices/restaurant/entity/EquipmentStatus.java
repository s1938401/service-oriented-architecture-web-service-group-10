package com.g10.microservices.restaurant.entity;

public enum EquipmentStatus {
    OK, BROKEN
}
