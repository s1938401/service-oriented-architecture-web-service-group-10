package com.g10.microservices.restaurant.entity;

public enum KitchenStatus {
    OPEN, CLOSED
}
