package com.g10.microservices.restaurant;

public class constants {
    public static final String DOMAIN = "www.braxburgers.com";

    public static final String RESTAURANT_SERVICE_ID = "restaurant-service";
    public static final String INVENTORY_SERVICE_ID = "inventory-service";
    public static final String HRM_SERVICE_ID = "hrm-service";
    public static final String WORKER_SERVICE_ID = "worker-service";

    public static final String ORDER_QUEUE = "orderQueue";
    public static final String PACK_QUEUE = "packQueue";

    public static final int COOKING_TIME_MILLI = 5000;
    public static final int PACK_TIME_MILLI = 5000;

    public static final String ORDER_DEBUG = "[ORDER PROCESSOR] ";
    public static final String PACK_DEBUG = "[PACK PROCESSOR] ";
}
