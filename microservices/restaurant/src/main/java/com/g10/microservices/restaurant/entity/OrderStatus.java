package com.g10.microservices.restaurant.entity;

public enum OrderStatus {
    TODO, START, READY, PACKED, CANCELLED, PICKEDUP
}
