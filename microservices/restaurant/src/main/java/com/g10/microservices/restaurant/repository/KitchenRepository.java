package com.g10.microservices.restaurant.repository;

import com.g10.microservices.restaurant.entity.Kitchen;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KitchenRepository extends JpaRepository<Kitchen, Long> {
    List<Kitchen> findAll();
}