package com.g10.microservices.restaurant.websocket;

import com.g10.microservices.restaurant.controller.FrontendController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class WebsocketHandler extends TextWebSocketHandler {
    @Autowired
    FrontendController frontendController;

    Map<String, WebSocketSession> sessions = new HashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String id = session.getId();
        sessions.put(id,session);
        System.out.println("New session: "+id);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String response = frontendController.processIncomingMessage(session.getId(),message.getPayload());
        session.sendMessage(new TextMessage(response));
    }

    public void sendMessageToSessionId(String id, String message) throws IOException {
        WebSocketSession session = sessions.get(id);
        session.sendMessage(new TextMessage(message));
    }

    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String id = session.getId();
        sessions.remove(id);
        frontendController.removeSessionOrders(id);
        System.out.println("Closed session: "+id);
    }
}
