package com.g10.microservices.restaurant.repository;

import com.g10.microservices.restaurant.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment,Long> {
    List<Equipment> findAll();
    Equipment getById(Long id);
}