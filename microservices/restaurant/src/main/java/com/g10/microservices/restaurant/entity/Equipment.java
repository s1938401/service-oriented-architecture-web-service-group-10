package com.g10.microservices.restaurant.entity;

import java.util.List;
import javax.persistence.*;

@Entity
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ElementCollection
    private List<Long> productIds;

    @Enumerated(EnumType.STRING)
    private EquipmentStatus status;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setProducts(List<Long> products) {
        this.productIds = products;
    }

    public List<Long> getProducts() {
        return productIds;
    }

    public void setStatus(EquipmentStatus status) {
        this.status = status;
    }

    public EquipmentStatus getStatus() {
        return status;
    }
}