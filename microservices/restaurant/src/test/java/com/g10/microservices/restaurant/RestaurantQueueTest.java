package com.g10.microservices.restaurant;

import com.g10.microservices.restaurant.entity.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.g10.microservices.restaurant.constants.*;
import static org.junit.Assert.*;

public class RestaurantQueueTest extends RestaurantApplicationTests {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    // Too much has changed with Kubernetes and Refactor of queues. Beforehand it worked.
    @Test
    @Ignore
    public void customFunctionQueue() throws Exception {
        //Get inventory
        String invUri = "/product";
        MvcResult invResponse = mvc.perform(MockMvcRequestBuilders.get(invUri)).andReturn();
        int invStatus = invResponse.getResponse().getStatus();
        assertEquals(200, invStatus);
        String oldInvString = super.mapToJson(invResponse.getResponse().getContentAsString());

        // Create order.
        String postUri = "/order";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // Create 2nd order.
        MvcResult res2 = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        String createContent2 = res2.getResponse().getContentAsString();
        Order createdOrder2 = super.mapFromJson(createContent2, Order.class);
        Long id2 = createdOrder2.getId();

        // Check if started
        String uri = "/order/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        // Check if 2nd order is in queue
        String urib = "/order/" + id2;
        MvcResult mvcResultb = mvc.perform(MockMvcRequestBuilders.get(urib)).andReturn();
        int statusb = mvcResultb.getResponse().getStatus();
        assertEquals(200, statusb);

        String finishContentb = mvcResultb.getResponse().getContentAsString();
        Order finishOrderb = super.mapFromJson(finishContentb, Order.class);
        assertEquals(OrderStatus.TODO, finishOrderb.getStatus());


        TimeUnit.MILLISECONDS.sleep(COOKING_TIME_MILLI*3+100);

        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status2 = mvcResult2.getResponse().getStatus();
        assertEquals(200, status2);

        String finishContent2 = mvcResult2.getResponse().getContentAsString();
        Order finishOrder2 = super.mapFromJson(finishContent2, Order.class);
//        assertEquals(OrderStatus.READY, finishOrder2.getStatus());

        TimeUnit.MILLISECONDS.sleep(PACK_TIME_MILLI+100);

        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status3 = mvcResult3.getResponse().getStatus();
        assertEquals(200, status3);

        String finishContent3 = mvcResult3.getResponse().getContentAsString();
        Order finishOrder3 = super.mapFromJson(finishContent3, Order.class);
        assertEquals(OrderStatus.PACKED, finishOrder3.getStatus());

        //Get inventory
        MvcResult invResponse2 = mvc.perform(MockMvcRequestBuilders.get(invUri)).andReturn();
        int invStatus2 = invResponse2.getResponse().getStatus();
        assertEquals(200, invStatus2);
        String newInvString = super.mapToJson(invResponse2.getResponse().getContentAsString());

        System.out.println(oldInvString);
        System.out.println(newInvString);

        // Waitout second order
        MvcResult mvcResultb2 = mvc.perform(MockMvcRequestBuilders.get(urib)).andReturn();
        int statusb2 = mvcResultb2.getResponse().getStatus();
        assertEquals(200, statusb2);

        TimeUnit.MILLISECONDS.sleep(COOKING_TIME_MILLI*3+PACK_TIME_MILLI+1000);
        String finishContentb2 = mvcResultb2.getResponse().getContentAsString();
        Order finishOrderb2 = super.mapFromJson(finishContentb2, Order.class);
        assertEquals(OrderStatus.PACKED, finishOrderb2.getStatus());

        assertNotEquals(oldInvString,newInvString);
    }
}
