package com.g10.microservices.restaurant;

import com.g10.microservices.restaurant.entity.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RestaurantControllerTest extends RestaurantApplicationTests {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getKitchenStatus() throws Exception {
        String uri = "/kitchen/status";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        String kStatus = content.replaceAll("\"", "");
        assertTrue(kStatus.equals("OPEN") || kStatus.equals("CLOSED"));
    }

    @Test
    @Ignore
    public void createOrder() throws Exception {
        // Create order
        String uri = "/order/create/testorder";
        Order order = new Order();
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        // Check if successful
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(content, Order.class);
        assertEquals(OrderStatus.TODO, createdOrder.getStatus());
        assertEquals("1,2,3", createdOrder.getProducts());
        Long id = createdOrder.getId();

        // Delete order
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void getAllOrders() throws Exception {
        // Empty list if no orders present
        String uri = "/order";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Order[] allOrders = super.mapFromJson(content, Order[].class);
        assertEquals(0, allOrders.length);

        // Create order
        String createUri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult firstCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String firstCreatedContent = firstCreated.getResponse().getContentAsString();
        Order firstCreatedOrder = super.mapFromJson(firstCreatedContent, Order.class);
        Long firstId = firstCreatedOrder.getId();

        // Now list must have length one.
        MvcResult oneMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int oneStatus = oneMvcResult.getResponse().getStatus();
        assertEquals(200, oneStatus);
        String oneContent = oneMvcResult.getResponse().getContentAsString();
        System.out.println(oneContent);
        Order[] oneAllOrders = super.mapFromJson(oneContent, Order[].class);
        assertEquals(1, oneAllOrders.length);

        // Create another order.
        Order secondOrder = new Order();
        secondOrder.setStatus(OrderStatus.TODO);
        secondOrder.setProducts("1,2,3");
        String secondInputJson = super.mapToJson(secondOrder);
        MvcResult secondCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(secondInputJson)).andReturn();


        String secondCreatedContent = secondCreated.getResponse().getContentAsString();
        Order secondCreatedOrder = super.mapFromJson(secondCreatedContent, Order.class);
        Long secondId = secondCreatedOrder.getId();

        // Now list must have length two.
        MvcResult secondMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int twoStatus = secondMvcResult.getResponse().getStatus();
        assertEquals(200, twoStatus);
        String twoContent = secondMvcResult.getResponse().getContentAsString();
        Order[] twoAllOrders = super.mapFromJson(twoContent, Order[].class);
        assertEquals(2, twoAllOrders.length);

        // Delete orders.
        String deleteUri = "/order/" + firstId;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        String secondDeleteUri = "/order/" + secondId;
        MvcResult secondDeleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(secondDeleteUri)).andReturn();
        int secondDeleteStatus = secondDeleteMvcResult.getResponse().getStatus();
        assertEquals(204, secondDeleteStatus);
    }

    @Test
    @Ignore
    public void getOrderById() throws Exception {
        // Non-existent Id should result in 404 error.
        String badUri = "/order/111";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Id of the order is automatically set by the database (starting from 1)
        // Create order
        String uri = "/order/create/testorder";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // Get order by Id and check result.
        String idUri = "/order/" + id;
        MvcResult idMvcResult = mvc.perform(MockMvcRequestBuilders.get(idUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = idMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = idMvcResult.getResponse().getContentAsString();
        Order receivedOrder = super.mapFromJson(content, Order.class);
        assertEquals(OrderStatus.TODO, receivedOrder.getStatus());
        assertEquals("1,2,3", receivedOrder.getProducts());

        // Delete order.
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void updateOrder() throws Exception {
        // Create order.
        String uri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // Non-existing id should result in 404 error.
        String badUri = "/order/1000";
        Order badOrder = new Order();
        badOrder.setStatus(OrderStatus.READY);
        badOrder.setProducts("1,3");
        String badInputJson = super.mapToJson(badOrder);
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.put(badUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(badInputJson)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Update order and check result.
        String updateUri = "/order/" + id;
        Order updatedOrder = new Order();
        updatedOrder.setStatus(OrderStatus.PACKED);
        updatedOrder.setProducts("1,3");
        String updateInputJson = super.mapToJson(updatedOrder);
        MvcResult updateMvcResult = mvc.perform(MockMvcRequestBuilders.put(updateUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updateInputJson)).andReturn();

        int status = updateMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = updateMvcResult.getResponse().getContentAsString();
        Order receivedOrder = super.mapFromJson(content, Order.class);
        assertEquals(OrderStatus.PACKED, receivedOrder.getStatus());
        assertEquals("1,3", receivedOrder.getProducts());

        // Update order as to-do so it can be deleted
        String todoUri = "/order/" + id + "/test";
        MvcResult todoMvcResult = mvc.perform(MockMvcRequestBuilders.put(todoUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        int todoStatus = todoMvcResult.getResponse().getStatus();
        assertEquals(200, todoStatus);

        // Delete order.
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void deleteOrder() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/order/355";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.delete(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create order.
        String postUri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // Delete order.
        String uri = "/order/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);
    }

    @Test
    public void getKitchen() throws Exception {
        String uri = "/kitchen";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        Kitchen kitchen = super.mapFromJson(content, Kitchen.class);
        assertEquals(KitchenStatus.OPEN, kitchen.getStatus());
        assertEquals(Long.valueOf(1), kitchen.getId());
    }

    @Test
    public void getAllEquipment() throws Exception {
        // Empty list if no equipment present
        String uri = "/kitchen/equipment";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Equipment[] allEquipment = super.mapFromJson(content, Equipment[].class);
        assertEquals(3, allEquipment.length);

        // Create equipment.
        String createUri = "/kitchen/equipment";
        Equipment equipment = new Equipment();
        equipment.setStatus(EquipmentStatus.OK);
        equipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String inputJson = super.mapToJson(equipment);
        MvcResult firstCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String firstCreatedContent = firstCreated.getResponse().getContentAsString();
        Equipment firstCreatedEquipment = super.mapFromJson(firstCreatedContent, Equipment.class);
        Long firstId = firstCreatedEquipment.getId();

        // Now list must have length one.
        MvcResult oneMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int oneStatus = oneMvcResult.getResponse().getStatus();
        assertEquals(200, oneStatus);
        String oneContent = oneMvcResult.getResponse().getContentAsString();
        System.out.println(oneContent);
        Equipment[] oneAllEquipment = super.mapFromJson(oneContent, Equipment[].class);
        assertEquals(4, oneAllEquipment.length);

        // Create another equipment.
        Equipment secondEquipment = new Equipment();
        secondEquipment.setStatus(EquipmentStatus.BROKEN);
        secondEquipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String secondInputJson = super.mapToJson(secondEquipment);
        MvcResult secondCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(secondInputJson)).andReturn();


        String secondCreatedContent = secondCreated.getResponse().getContentAsString();
        Equipment secondCreatedEquipment = super.mapFromJson(secondCreatedContent, Equipment.class);
        Long secondId = secondCreatedEquipment.getId();

        // Now list must have length two.
        MvcResult secondMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int twoStatus = secondMvcResult.getResponse().getStatus();
        assertEquals(200, twoStatus);
        String twoContent = secondMvcResult.getResponse().getContentAsString();
        Equipment[] twoAllEquipment = super.mapFromJson(twoContent, Equipment[].class);
        assertEquals(5, twoAllEquipment.length);

        // Delete all equipment.
        String deleteUri = "/kitchen/equipment/" + firstId;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        String secondDeleteUri = "/kitchen/equipment/" + secondId;
        MvcResult secondDeleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(secondDeleteUri)).andReturn();
        int secondDeleteStatus = secondDeleteMvcResult.getResponse().getStatus();
        assertEquals(204, secondDeleteStatus);
    }

    @Test
    public void getEquipmentById() throws Exception {
        // Non-existent Id should result in 404 error.
        String badUri = "/kitchen/equipment/111";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create equipment
        String uri = "/kitchen/equipment";
        Equipment equipment = new Equipment();
        equipment.setStatus(EquipmentStatus.OK);
        equipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String inputJson = super.mapToJson(equipment);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Equipment createdEquipment = super.mapFromJson(createContent, Equipment.class);
        Long id = createdEquipment.getId();

        // Get equipment by Id and check result.
        String idUri = "/kitchen/equipment/" + id;
        MvcResult idMvcResult = mvc.perform(MockMvcRequestBuilders.get(idUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = idMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = idMvcResult.getResponse().getContentAsString();
        Equipment receivedEquipment = super.mapFromJson(content, Equipment.class);
        assertEquals(EquipmentStatus.OK, receivedEquipment.getStatus());
        assertEquals(Arrays.asList(1L, 2L, 3L), receivedEquipment.getProducts());

        // Delete equipment.
        String deleteUri = "/kitchen/equipment/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void completeOrder() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/order/355/complete";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create order.
        String postUri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // No longer works due to queue and restriction of status
//        // Finish the order
//        String uri = "/order/" + id + "/complete";
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);

//        String finishContent = mvcResult.getResponse().getContentAsString();
//        Order finishOrder = super.mapFromJson(finishContent, Order.class);
//        assertEquals(OrderStatus.PACKED, finishOrder.getStatus());

        // Update order as to-do so it can be deleted
        String todoUri = "/order/" + id + "/test";
        Order todoOrder = new Order();
        todoOrder.setStatus(OrderStatus.TODO);
        todoOrder.setProducts("1, 2, 3");
        String todoInputJson = super.mapToJson(todoOrder);
        MvcResult todoMvcResult = mvc.perform(MockMvcRequestBuilders.put(todoUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(todoInputJson)).andReturn();
        int todoStatus = todoMvcResult.getResponse().getStatus();
        assertEquals(200, todoStatus);

        // Delete order.
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void readyOrder() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/order/355/ready";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create order.
        String postUri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // No longer works due to queue and restrictions.
//        // Set the order to ready
//        String uri = "/order/" + id + "/ready";
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);
//
//        String finishContent = mvcResult.getResponse().getContentAsString();
//        Order finishOrder = super.mapFromJson(finishContent, Order.class);
//        assertEquals(OrderStatus.READY, finishOrder.getStatus());

        // Update order as to-do so it can be deleted
        Order todoOrder = new Order();
        todoOrder.setStatus(OrderStatus.TODO);
        todoOrder.setProducts("1, 2, 3");
        String todoInputJson = super.mapToJson(todoOrder);
        String todoUri = "/order/" + id + "/test";
        MvcResult todoMvcResult = mvc.perform(MockMvcRequestBuilders.put(todoUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(todoInputJson)).andReturn();
        int todoStatus = todoMvcResult.getResponse().getStatus();
        assertEquals(200, todoStatus);

        // Delete order.
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    @Ignore
    public void startOrder() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/order/355/start";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create order.
        String postUri = "/order/create/test";
        Order order = new Order();
        order.setStatus(OrderStatus.TODO);
        order.setProducts("1,2,3");
        String inputJson = super.mapToJson(order);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Order createdOrder = super.mapFromJson(createContent, Order.class);
        Long id = createdOrder.getId();

        // No longer workers due to queue and restrictions.
//        // Start the order
//        String uri = "/order/" + id + "/start";
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);
//
//        String finishContent = mvcResult.getResponse().getContentAsString();
//        Order finishOrder = super.mapFromJson(finishContent, Order.class);
//        assertEquals(OrderStatus.START, finishOrder.getStatus());

        // Update order as to-do so it can be deleted
        String todoUri = "/order/" + id + "/test";
        MvcResult todoMvcResult = mvc.perform(MockMvcRequestBuilders.put(todoUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        int todoStatus = todoMvcResult.getResponse().getStatus();
        assertEquals(200, todoStatus);

        // Delete order.
        String deleteUri = "/order/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void fixEquipment() throws Exception {
        // Non-existent Id should result in 404 error.
        String badUri = "/kitchen/equipment/111/fix";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create equipment
        String uri = "/kitchen/equipment";
        Equipment equipment = new Equipment();
        equipment.setStatus(EquipmentStatus.BROKEN);
        equipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String inputJson = super.mapToJson(equipment);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Equipment createdEquipment = super.mapFromJson(createContent, Equipment.class);
        Long id = createdEquipment.getId();

        // Fix the equipment.
        String fixUri = "/kitchen/equipment/" + id + "/fix";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(fixUri)).andReturn();

        String fixedContent = mvcResult.getResponse().getContentAsString();
        Equipment fixedEquipment = super.mapFromJson(fixedContent, Equipment.class);
        assertEquals(EquipmentStatus.OK, fixedEquipment.getStatus());

        // Delete equipment.
        String deleteUri = "/kitchen/equipment/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void createEquipment() throws Exception {
        // Create equipment
        String uri = "/kitchen/equipment";
        Equipment equipment = new Equipment();
        equipment.setStatus(EquipmentStatus.OK);
        equipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String inputJson = super.mapToJson(equipment);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        // Check if successful
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Equipment createdEquipment = super.mapFromJson(content, Equipment.class);
        assertEquals(EquipmentStatus.OK, createdEquipment.getStatus());
        assertEquals(Arrays.asList(1L, 2L, 3L), createdEquipment.getProducts());
        Long id = createdEquipment.getId();

        // Delete equipment
        String deleteUri = "/kitchen/equipment/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void deleteEquipment() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/kitchen/equipment/355";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.delete(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create equipment.
        String uri = "/kitchen/equipment";
        Equipment equipment = new Equipment();
        equipment.setStatus(EquipmentStatus.OK);
        equipment.setProducts(Arrays.asList(1L, 2L, 3L));
        String inputJson = super.mapToJson(equipment);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = mvcResult.getResponse().getContentAsString();
        Equipment createdEquipment = super.mapFromJson(createContent, Equipment.class);
        Long id = createdEquipment.getId();

        // Delete equipment.
        String deleteUri = "/kitchen/equipment/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }
}
