package com.g10.microservices.hrm.controller;

import com.g10.microservices.hrm.entity.Day;
import com.g10.microservices.hrm.entity.Employee;
import com.g10.microservices.hrm.repository.DayRepository;
import com.g10.microservices.hrm.repository.EmployeeRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class HRMController {

    @Autowired
    private DayRepository dayRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @ApiOperation(
            value = "Get all day objects",
            notes = "Get a list of all days in the database. They contain a list of employee for each day."
    )
    @GetMapping("/admin/schedule")
    @ResponseBody
    public List<Day> getSchedule(){
        return dayRepository.findAll();
    }

    @ApiOperation(
            value = "Post a new employee",
            notes = "Hire a new employee, requires employee object to be saved."
    )
    @PostMapping("/admin/employee")
    @ResponseBody
    public Employee addEmployee(@RequestBody Employee newEmployee) {
        return employeeRepository.save(newEmployee);
    }

    @ApiOperation(
            value = "Delete an employee objects by ID",
            notes = "Fire an employee from the database."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The employee with the specified id does not exists in the database"),
    })
    @DeleteMapping("/admin/employee/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> removeEmployee(@PathVariable Long id) {
        Employee employee = employeeRepository.getById(id);
        if (employee != null) {
            employeeRepository.delete(employee);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get all employee objects",
            notes = "Get a list of all employees in the database."
    )
    @GetMapping("/admin/employee")
    @ResponseBody
    public List<Employee> getEmployee() {
        return employeeRepository.findAll();
    }

    @ApiOperation(
            value = "Get an employee objects by ID",
            notes = "Get an employee from the database."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The employee with the specified id does not exists in the database"),
    })
    @GetMapping("/admin/employee/{id}")
    @ResponseBody
    public ResponseEntity<Employee> getEmployee(@PathVariable Long id) {
        Employee employee = employeeRepository.getById(id);
        if (employee != null) {
            return ResponseEntity.ok(employee);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Update an employee",
            notes = "Provide an id and an employee object that will update the current employee with the that id."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The employee with the specified id does not exists in the database"),
    })
    @PutMapping("/admin/employee/{id}")
    @ResponseBody
    public ResponseEntity<Employee> editEmployee(@PathVariable Long id, @RequestBody Employee newEmployee) {
        Employee oldEmployee = employeeRepository.getById(id);
        if (oldEmployee != null) {
            // TODO: maybe copy all old attributes not given in newEmployee?
            newEmployee.setId(oldEmployee.getId());
            return ResponseEntity.ok(employeeRepository.save(newEmployee));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get the employees of a day",
            notes = "Get the list of all employees of a day, should be used together with getSchedule()"
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The day with the specified id does not exists in the database"),
    })
    @GetMapping("/admin/schedule/{id}")
    @ResponseBody
    public ResponseEntity<Day> getDay(@PathVariable Long id) {
        Day day = dayRepository.getById(id);
        if (day != null) {
            return ResponseEntity.ok(day);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Update a day",
            notes = "Provide an id and an day object that will update the current employee with the that id."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The day with the specified id does not exists in the database"),
    })
    @PutMapping("/admin/schedule/{id}")
    @ResponseBody
    public ResponseEntity<Day> editDay(@PathVariable Long id, @RequestBody Day newDay) {
        Day oldDay = dayRepository.getById(id);
        if (oldDay != null) {
            newDay.setId(oldDay.getId());
            return ResponseEntity.ok(dayRepository.save(newDay));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Generate new schedule.",
            notes = "Automatically generates (and overwrites the existing) schedule."
    )
    @GetMapping("/admin/schedule/create/")
    @ResponseBody
    public List<Day> createSchedule(){
        List<Day> schedule = dayRepository.findAll();
        List<Employee> employees = employeeRepository.findAll();
        for(int i = 1; i < 8; i++){
            dayRepository.getById((long) i).emptyDay();
            int amount = 0;
            Collections.shuffle(employees);
            for (Employee employee : employees){
                // Add 3 employees to each day.
                if (amount++ >= 3) {
                    dayRepository.save(schedule.get(i-1));
                    break;
                }
                schedule.get(i-1).addEmployee(employee);
            }
        }
        return schedule;
    }
}
