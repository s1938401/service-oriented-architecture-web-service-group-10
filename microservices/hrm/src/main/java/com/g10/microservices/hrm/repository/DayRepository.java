package com.g10.microservices.hrm.repository;

import com.g10.microservices.hrm.entity.Day;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DayRepository extends JpaRepository<Day,Long> {
    List<Day> findAll();
    Day getById(Long id);
}
