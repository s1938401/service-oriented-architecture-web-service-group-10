package com.g10.microservices.hrm.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.g10.microservices.hrm.entity.Employee;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "schedule")
public class Day {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToMany
    List<Employee> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        id = Id;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void emptyDay(){employees.clear();}

    public void addEmployee(Employee employee) {employees.add(employee);}
}
