package com.g10.microservices.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class HRMApplication {

	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.g10.microservices.hrm"))
				.build()
				.apiInfo(apiDetails())
				.useDefaultResponseMessages(false);
	}

	private ApiInfo apiDetails() {
		String description = "This is documentation for the HRM service of the Brax Burgers application. "
				+ "This is a project for the course Service-Oriented Architectures with Webservices. "
				+ "Made by students from University of Twente: Max Lievense and Bram Dekker.";

		return new ApiInfoBuilder()
				.title("HRM Service API Documentation")
				.description(description)
				.version("1.0.0")
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(HRMApplication.class, args);
	}

}
