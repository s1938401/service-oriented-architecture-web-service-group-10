package com.g10.microservices.hrm.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name){name = Name;}
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String Phone){phone = Phone;}
}
