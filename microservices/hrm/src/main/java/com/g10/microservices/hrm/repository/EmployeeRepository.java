package com.g10.microservices.hrm.repository;

import com.g10.microservices.hrm.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    List<Employee> findAll();
    Employee getById(Long id);
}
