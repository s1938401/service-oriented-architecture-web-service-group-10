INSERT INTO employees (id, name, phone) VALUES
    (1, 'Bram', '06111111111'),
    (2, 'Max', '06111111111'),
    (3, 'Robot1', '06111111111'),
    (4, 'Robot2', '06111111111'),
    (5, 'Robot3', '06111111111'),
    (6, 'Robot4', '06111111111'),
    (7, 'Robot5', '06111111111'),
    (8, 'Robot6', '06111111111'),
    (9, 'Robot7', '06111111111'),
    (10, 'Robot8', '06111111111');

INSERT INTO schedule (id) VALUES
    (1),
    (2),
    (3),
    (4),
    (5),
    (6),
    (7);