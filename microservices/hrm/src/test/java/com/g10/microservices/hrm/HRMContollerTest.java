package com.g10.microservices.hrm;

import com.g10.microservices.hrm.entity.Day;
import com.g10.microservices.hrm.entity.Employee;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class HRMContollerTest extends HRMApplicationTests {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void AllEmployee() throws Exception {
        String oldName = "ABCD";
        String newName = "DCBA";
        String oldPhone = "0621111111";
        String newPhone = "0621111112";

        String uri = "/admin/employee";
        Employee employee = new Employee();
        employee.setName(oldName);
        employee.setPhone(oldPhone);
        String inputJson = super.mapToJson(employee);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        // Check if successful
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Employee createdEmployee = super.mapFromJson(content, Employee.class);
        assertEquals(oldName, createdEmployee.getName());
        assertEquals(oldPhone,createdEmployee.getPhone());
        Long id = createdEmployee.getId();

        // Edit employee
        String editUri = "/admin/employee/" + id;
        createdEmployee.setName(newName);
        createdEmployee.setPhone(newPhone);
        String inputJson2 = super.mapToJson(createdEmployee);
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.put(editUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson2)).andReturn();
        int status2 = mvcResult.getResponse().getStatus();
        assertEquals(200, status2);
        String newEmployeeString = mvcResult2.getResponse().getContentAsString();
        Employee newEmployee = super.mapFromJson(newEmployeeString,Employee.class);

        assertEquals(newName,newEmployee.getName());
        assertEquals(newPhone,newEmployee.getPhone());

        // Delete employee
        String deleteUri = "/admin/employee/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void makeSchedule() throws Exception {
        String uri = "/admin/schedule";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String contentOld = mvcResult.getResponse().getContentAsString();

        String uri2 = "/admin/schedule/create/";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status2 = mvcResult.getResponse().getStatus();
        assertEquals(200, status2);
        String contentNew = mvcResult2.getResponse().getContentAsString();

        assertNotEquals(contentOld,contentNew);
    }

    @Test
    public void editDay() throws Exception {
        String uri = "/admin/schedule/1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String contentOld = mvcResult.getResponse().getContentAsString();

        String uri2 = "/admin/employee/1";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status2 = mvcResult.getResponse().getStatus();
        assertEquals(200, status2);
        String employeeString = mvcResult2.getResponse().getContentAsString();
        Employee employee = super.mapFromJson(employeeString,Employee.class);

        String uri3 = "/admin/schedule/1";
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uri3)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status3 = mvcResult.getResponse().getStatus();
        assertEquals(200, status3);
        String dayString = mvcResult3.getResponse().getContentAsString();
        Day day = super.mapFromJson(dayString,Day.class);

        day.addEmployee(employee);
        assertEquals(day.getEmployees().get(0),employee);

        String uri4 = "/admin/schedule/1";
        MvcResult mvcResult4 = mvc.perform(MockMvcRequestBuilders.put(uri4)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status4 = mvcResult.getResponse().getStatus();
        assertEquals(200, status4);
        String contentNew = mvcResult4.getResponse().getContentAsString();

        assertNotEquals(contentOld,contentNew);
    }
}
