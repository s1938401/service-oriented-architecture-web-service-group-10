package com.g10.microservices.worker.repository;

import com.g10.microservices.worker.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findAll();
    Order getById(Long id);
}