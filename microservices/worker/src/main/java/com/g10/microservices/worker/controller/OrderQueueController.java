package com.g10.microservices.worker.controller;

import com.g10.microservices.worker.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.g10.microservices.worker.constants.*;


@Component
public class OrderQueueController {
    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private JmsTemplate jmsTemplate;


    @Autowired
    private WorkerController workerController;

    String inventoryEndpoint = "http://" + INVENTORY_SERVICE_ID + "/worker/product/";
    String restaurantEndpoint = "http://" + RESTAURANT_SERVICE_ID + "/order/";

    @JmsListener(destination = ORDER_QUEUE)
    public void onMessage(String message) {
        System.out.println(ORDER_DEBUG + "Received message { " + message +" }");
        Long id = Long.parseLong(message);
        try {
            ResponseEntity<Order> orderResponse = this.restTemplate.exchange(restaurantEndpoint + message + "/start/",
                    HttpMethod.GET, null, Order.class);
            Order order = Objects.requireNonNull(orderResponse.getBody());
            workerController.createOrder(order);
            String products = order.getProducts();
            startOrder(id, products);

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void startOrder(Long id, String order) throws InterruptedException {
        for (String i : order.split(",")) {
            System.out.println(ORDER_DEBUG + "for { " + id.toString() + " } - making { " + i + "}");
            this.restTemplate.exchange(inventoryEndpoint+i+"/decr/", HttpMethod.GET, null, Void.class);
            TimeUnit.MILLISECONDS.sleep(COOKING_TIME_MILLI);
        }
        workerController.readyOrder(id);
        jmsTemplate.convertAndSend(PACK_QUEUE, id.toString());
    }
}
