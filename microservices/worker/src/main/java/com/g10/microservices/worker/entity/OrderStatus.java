package com.g10.microservices.worker.entity;

public enum OrderStatus {
    TODO, START, READY, PACKED, PICKEDUP
}
