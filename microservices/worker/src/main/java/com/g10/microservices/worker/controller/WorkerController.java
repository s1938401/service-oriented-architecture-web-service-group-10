package com.g10.microservices.worker.controller;

import com.g10.microservices.worker.entity.Order;
import com.g10.microservices.worker.entity.OrderStatus;
import com.g10.microservices.worker.repository.OrderRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.g10.microservices.worker.constants.*;

import java.util.List;

@RestController
public class WorkerController {
    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    OrderRepository orderRepository;

    @ApiOperation(
            value = "Receive an order",
            notes = "Provide an order object and add a order to this worker. The order also gets sent to a message queue for processing it."
    )
    @PostMapping("/order")
    @ResponseBody
    public Order createOrder(@RequestBody Order newOrder){
        newOrder.setStatus(OrderStatus.START);
        Order createdOrder = orderRepository.save(newOrder);
        jmsTemplate.convertAndSend(ORDER_QUEUE, createdOrder.getId());
        return createdOrder;
    }

    @ApiOperation(
            value = "Mark order as ready",
            notes = "Provide an id and mark the order with that id as ready by setting its status."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The order is not of status START."),
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}/ready")
    public ResponseEntity<Order> readyOrder(@PathVariable Long id){
        Order order = orderRepository.getById(id);
        if (order != null) {
            if (order.getStatus() != OrderStatus.START){
                return ResponseEntity.badRequest().build();
            }
            order.setStatus(OrderStatus.READY);
            return ResponseEntity.ok(orderRepository.save(order));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get an order by id",
            notes = "Provide an id and get information about that specific order. Returns not-found error if the id is not present."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The order with the specified id does not exists in the database"),
    })
    @GetMapping("/order/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable Long id){
        Order order = orderRepository.getById(id);
        if (order != null) {
            return ResponseEntity.ok(order);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get all orders",
            notes = "Get a list with all orders in the database."
    )
    @GetMapping("/order")
    @ResponseBody
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
