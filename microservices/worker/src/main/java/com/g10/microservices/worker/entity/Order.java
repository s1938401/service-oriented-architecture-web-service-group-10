package com.g10.microservices.worker.entity;

import com.g10.microservices.worker.entity.OrderStatus;

import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "purchase")
public class Order {

    @Id
    private Long id;

    private String productIds;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    private String sessionId;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setProducts(String products) {
        this.productIds = products;
    }

    public String getProducts() {
        return productIds;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setSessionId(String SessionId) {this.sessionId = SessionId;}

    public String getSessionId() {return sessionId;}
}
