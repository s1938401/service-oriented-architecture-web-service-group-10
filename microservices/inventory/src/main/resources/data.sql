-- product (id bigint not null, in_queue boolean, name varchar(255), stock bigint, retailer_id bigint, primary key (id))
-- retailer (id bigint not null, phone varchar(255), primary key (id))

INSERT INTO retailer (id, phone) VALUES
    (1, '06 11111111'),
    (2, '06 22222222'),
    (3, '06 33333333');

INSERT INTO product (id, name, stock, retailer_id) VALUES
    (1, 'Fries', 50, 1),
    (2, 'Burger', 100, 2),
    (3, 'Chicken burger', 100, 2),
    (4, 'Curly fries', 25, 1),
    (5, 'Bacon', 150, 2),
    (6, 'Lemonade', 35, 3),
    (7, 'Milkshake', 15, 3),
    (8, 'Soda', 80, 3),
    (9, 'Sunday icecream', 0, 3);