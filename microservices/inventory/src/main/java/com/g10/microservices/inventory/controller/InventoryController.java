package com.g10.microservices.inventory.controller;

import com.g10.microservices.inventory.entity.Product;
import com.g10.microservices.inventory.entity.Retailer;
import com.g10.microservices.inventory.repository.ProductRepository;
import com.g10.microservices.inventory.repository.RetailerRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoryController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    RetailerRepository retailerRepository;

    @ApiOperation(
            value = "Get all products",
            notes = "Get all products from the database"
    )
    @GetMapping("/admin/product")
    @ResponseBody
    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    @ApiOperation(
            value = "Get a product by id",
            notes = "Provide an id and get the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @GetMapping("/admin/product/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id) {
        Product product = productRepository.getById(id);
        if (product != null) {
            return ResponseEntity.ok(product);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Create a product for a specific retailer",
            notes = "Provide an id and a product object and create the new product at the retailer with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The retailer with the specified id does not exists in the database"),
    })
    @PostMapping("/admin/retailer/{id}/product")
    public ResponseEntity<Product> createProduct(@PathVariable Long id, @RequestBody Product newProduct) {
        Retailer retailer = retailerRepository.getById(id);
        if (retailer != null) {
            newProduct.setRetailer(retailer);
            return ResponseEntity.ok(productRepository.save(newProduct));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Update a product",
            notes = "Provide an id and a product object and update the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @PutMapping("/admin/product/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable Long id, @RequestBody Product newProduct) {
        Product oldProduct = productRepository.getById(id);
        if (oldProduct != null) {
            newProduct.setId(oldProduct.getId());
            return ResponseEntity.ok(productRepository.save(newProduct));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Delete a product",
            notes = "Provide an id and delete the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @DeleteMapping("/admin/product/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        Product product = productRepository.getById(id);
        if (product != null) {
            productRepository.delete(product);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Get all retailers",
            notes = "Get all retailers from the database."
    )
    @GetMapping("admin/retailer")
    @ResponseBody
    public List<Retailer> getAllRetailers() {
        return retailerRepository.findAll();
    }

    @ApiOperation(
            value = "Get a retailer by id",
            notes = "Provide an id and get the retailer with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The retailer with the specified id does not exists in the database"),
    })
    @GetMapping("admin/retailer/{id}")
    public ResponseEntity<Retailer> getRetailerById(@PathVariable Long id) {
        Retailer retailer = retailerRepository.getById(id);
        if (retailer != null) {
            return ResponseEntity.ok(retailer);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Increase the stock of a product",
            notes = "Provide an id and a number and increase the stock with number of the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The number to be increased by was not a positive integer"),
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @GetMapping("/admin/product/{id}/inc/{n}")
    public ResponseEntity<Product> increaseProduct(@PathVariable Long id, @PathVariable Long n) {
        if (n < 0) {
            return ResponseEntity.badRequest().build();
        }

        Product product = productRepository.getById(id);
        if (product != null) {
            Long oldStock = product.getStock();
            product.setStock(oldStock + n);
            return ResponseEntity.ok(productRepository.save(product));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Create a retailer",
            notes = "Provide a retailer object and create the new retailer."
    )
    @PostMapping("/admin/retailer")
    @ResponseBody
    public Retailer createRetailer(@RequestBody Retailer newRetailer) {
        return retailerRepository.save(newRetailer);
    }

    @ApiOperation(
            value = "Update a retailer",
            notes = "Provide an id and a retailer object and update the retailer with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The retailer with the specified id does not exists in the database"),
    })
    @PutMapping("/admin/retailer/{id}")
    public ResponseEntity<Retailer> updateRetailer(@PathVariable Long id, @RequestBody Retailer newRetailer) {
        Retailer oldRetailer = retailerRepository.getById(id);
        if (oldRetailer != null) {
            newRetailer.setId(oldRetailer.getId());
            return ResponseEntity.ok(retailerRepository.save(newRetailer));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Delete a retailer",
            notes = "Provide an id and delete the retailer with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "The retailer with the specified id does not exists in the database"),
    })
    @DeleteMapping("/admin/retailer/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteRetailer(@PathVariable Long id) {
        Retailer retailer = retailerRepository.getById(id);
        if (retailer != null) {
            retailerRepository.delete(retailer);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Decrement the stock of a product",
            notes = "Provide an id and decrement the stock of the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The stock cannot be decremented to a negative value"),
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @GetMapping("/admin/product/{id}/decr")
    public ResponseEntity<Product> decrementProduct(@PathVariable Long id) {
        Product product = productRepository.getById(id);
        if (product != null) {
            Long oldStock = product.getStock();
            if (oldStock < 1) {
                return ResponseEntity.badRequest().build();
            }
            product.setStock(oldStock - 1);
            return ResponseEntity.ok(productRepository.save(product));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(
            value = "Decrement the stock of a product as a worker",
            notes = "Provide an id and decrement the stock of the product with that id if it exists."
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "The stock cannot be decremented to a negative value"),
            @ApiResponse(code = 404, message = "The product with the specified id does not exists in the database"),
    })
    @GetMapping("/worker/product/{id}/decr")
    public ResponseEntity<Void> decrementProductWorker(@PathVariable Long id) {
        Product product = productRepository.getById(id);
        if (product != null) {
            Long oldStock = product.getStock();
            if (oldStock < 1) {
                return ResponseEntity.badRequest().build();
            }
            product.setStock(oldStock - 1);
            productRepository.save(product);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
