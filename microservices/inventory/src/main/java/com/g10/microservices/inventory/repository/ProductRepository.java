package com.g10.microservices.inventory.repository;

import com.g10.microservices.inventory.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long> {
    List<Product> findAll();
    Product getById(Long id);
}