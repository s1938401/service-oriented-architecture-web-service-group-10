package com.g10.microservices.inventory.repository;

import com.g10.microservices.inventory.entity.Retailer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RetailerRepository extends JpaRepository<Retailer, Long> {
    List<Retailer> findAll();
    Retailer getById(Long id);

}
