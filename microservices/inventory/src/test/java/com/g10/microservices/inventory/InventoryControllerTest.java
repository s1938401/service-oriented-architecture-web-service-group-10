package com.g10.microservices.inventory;

import com.g10.microservices.inventory.entity.Product;
import com.g10.microservices.inventory.entity.Retailer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class InventoryControllerTest extends InventoryApplicationTests {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void createProduct() throws Exception {
        // Non-existing id should result in a 404 error.
        String badUri = "/admin/retailer/1000/product";
        Product badProduct = new Product();
        badProduct.setName("fries");
        badProduct.setStock(50L);
        String badInputJson = super.mapToJson(badProduct);
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.post(badUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(badInputJson)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        // Check if successful
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(content, Product.class);
        Long id = createdProduct.getId();

        // Check if product is added to retailer.
        MvcResult removedMvcResult1 = mvc.perform(MockMvcRequestBuilders.get("/admin/retailer/" + retailerId)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int removedStatus1 = removedMvcResult1.getResponse().getStatus();
        assertEquals(200, removedStatus1);
        String removedContent1 = removedMvcResult1.getResponse().getContentAsString();
        Retailer removedRetailer1 = super.mapFromJson(removedContent1, Retailer.class);
        System.out.println(removedRetailer1.getProducts().get(0).getId());
        assertEquals(1, removedRetailer1.getProducts().size());

        // Delete product
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        // Check if product is also removed from retailer.
        MvcResult removedMvcResult = mvc.perform(MockMvcRequestBuilders.get("/admin/retailer/" + retailerId)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int removedStatus = removedMvcResult.getResponse().getStatus();
        assertEquals(200, removedStatus);
        String removedContent = removedMvcResult.getResponse().getContentAsString();
        Retailer removedRetailer = super.mapFromJson(removedContent, Retailer.class);
        assertEquals(0, removedRetailer.getProducts().size());

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void getAllProducts() throws Exception {
        // Empty list if no products present
        String uri = "/admin/product";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Product[] allProducts = super.mapFromJson(content, Product[].class);
        assertEquals(9, allProducts.length);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String createUri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String firstInputJson = super.mapToJson(product);
        MvcResult firstCreateResult = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(firstInputJson)).andReturn();

        String firstCreatedProductContent = firstCreateResult.getResponse().getContentAsString();
        Product firstCreatedProduct = super.mapFromJson(firstCreatedProductContent, Product.class);
        Long firstId = firstCreatedProduct.getId();

        // Now list must have length one.
        MvcResult oneMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int oneStatus = oneMvcResult.getResponse().getStatus();
        assertEquals(200, oneStatus);
        String oneContent = oneMvcResult.getResponse().getContentAsString();
        System.out.println(oneContent);
        Product[] oneAllProducts = super.mapFromJson(oneContent, Product[].class);
        assertEquals(10, oneAllProducts.length);

        // Create another product for retailer
        Product secondProduct = new Product();
        product.setName("burgers");
        product.setStock(80L);
        String secondInputJson = super.mapToJson(product);
        MvcResult secondCreateResult = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(secondInputJson)).andReturn();

        String secondCreatedProductContent = secondCreateResult.getResponse().getContentAsString();
        Product secondCreatedProduct = super.mapFromJson(secondCreatedProductContent, Product.class);
        Long secondId = secondCreatedProduct.getId();

        // Now list must have length two.
        MvcResult secondMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int twoStatus = secondMvcResult.getResponse().getStatus();
        assertEquals(200, twoStatus);
        String twoContent = secondMvcResult.getResponse().getContentAsString();
        Product[] twoAllProducts = super.mapFromJson(twoContent, Product[].class);
//        assertEquals(9, twoAllProducts.length);

        // Delete products.
        String deleteUri = "/admin/product/" + firstId;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        String secondDeleteUri = "/admin/product/" + secondId;
        MvcResult secondDeleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(secondDeleteUri)).andReturn();
        int secondDeleteStatus = secondDeleteMvcResult.getResponse().getStatus();
        assertEquals(204, secondDeleteStatus);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void getProductById() throws Exception {
        // Non-existent Id should result in 404 error.
        String badUri = "/admin/product/111";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Id of the product is automatically set by the database (starting from 1)
        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createdProductContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(createdProductContent, Product.class);
        Long id = createdProduct.getId();

        // Get product by Id and check result.
        String idUri = "/admin/product/" + id;
        MvcResult idMvcResult = mvc.perform(MockMvcRequestBuilders.get(idUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = idMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = idMvcResult.getResponse().getContentAsString();
        Product receivedProduct = super.mapFromJson(content, Product.class);
        assertEquals("fries", receivedProduct.getName());
        assertEquals(Long.valueOf(50), receivedProduct.getStock());

        // Delete product.
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void updateProduct() throws Exception {
        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createdProductContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(createdProductContent, Product.class);
        Long id = createdProduct.getId();

        // Non-existing id should result in 404 error.
        String badUri = "/admin/product/1000";
        Product badProduct = new Product();
        badProduct.setName("ketchup");
        badProduct.setStock(200L);
        String badInputJson = super.mapToJson(badProduct);
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.put(badUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(badInputJson)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Update product and check result.
        String updateUri = "/admin/product/" + id;
        Product updatedProduct = new Product();
        updatedProduct.setName("curly fries");
        updatedProduct.setStock(10L);
        String updateInputJson = super.mapToJson(updatedProduct);
        MvcResult updateMvcResult = mvc.perform(MockMvcRequestBuilders.put(updateUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updateInputJson)).andReturn();

        int status = updateMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = updateMvcResult.getResponse().getContentAsString();
        Product receivedProduct = super.mapFromJson(content, Product.class);
        assertEquals("curly fries", receivedProduct.getName());
        assertEquals(Long.valueOf(10), receivedProduct.getStock());

        // Delete product.
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void deleteProduct() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/admin/product/355";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.delete(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createdProductContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(createdProductContent, Product.class);
        Long id = createdProduct.getId();

        // Delete product.
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int status = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, status);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void createRetailer() throws Exception {
        // Create retailer
        String uri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String inputJson = super.mapToJson(retailer);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        // Check if successful
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(content, Retailer.class);
        assertEquals("06 12345678", createdRetailer.getPhone());
        assertEquals(Collections.emptyList(), createdRetailer.getProducts());
        Long id = createdRetailer.getId();

        // Delete retailer
        String deleteUri = "/admin/retailer/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void getAllRetailers() throws Exception {
        // Empty list if no retailers present
        String uri = "/admin/retailer";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Retailer[] allRetailers = super.mapFromJson(content, Retailer[].class);
//        assertEquals(4, allRetailers.length);

        // Create retailer
        String createUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String inputJson = super.mapToJson(retailer);
        MvcResult firstCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String firstCreatedContent = firstCreated.getResponse().getContentAsString();
        Retailer firstCreatedRetailer = super.mapFromJson(firstCreatedContent, Retailer.class);
        Long firstId = firstCreatedRetailer.getId();

        // Now list must have length one.
        MvcResult oneMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int oneStatus = oneMvcResult.getResponse().getStatus();
        assertEquals(200, oneStatus);
        String oneContent = oneMvcResult.getResponse().getContentAsString();
        System.out.println(oneContent);
        Retailer[] oneAllRetailers = super.mapFromJson(oneContent, Retailer[].class);
//        assertEquals(4, oneAllRetailers.length);

        // Create another retailer.
        Retailer secondRetailer = new Retailer();
        secondRetailer.setPhone("0251 333333");
        secondRetailer.setProducts(Collections.emptyList());
        String secondInputJson = super.mapToJson(secondRetailer);
        MvcResult secondCreated = mvc.perform(MockMvcRequestBuilders.post(createUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(secondInputJson)).andReturn();


        String secondCreatedContent = secondCreated.getResponse().getContentAsString();
        Retailer secondCreatedRetailer = super.mapFromJson(secondCreatedContent, Retailer.class);
        Long secondId = secondCreatedRetailer.getId();

        // Now list must have length two.
        MvcResult secondMvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int twoStatus = secondMvcResult.getResponse().getStatus();
        assertEquals(200, twoStatus);
        String twoContent = secondMvcResult.getResponse().getContentAsString();
        Retailer[] twoAllRetailers = super.mapFromJson(twoContent, Retailer[].class);
        assertEquals(5, twoAllRetailers.length);

        // Delete retailers.
        String deleteUri = "/admin/retailer/" + firstId;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        String secondDeleteUri = "/admin/retailer/" + secondId;
        MvcResult secondDeleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(secondDeleteUri)).andReturn();
        int secondDeleteStatus = secondDeleteMvcResult.getResponse().getStatus();
        assertEquals(204, secondDeleteStatus);
    }

    @Test
    public void getRetailerById() throws Exception {
        // Non-existent Id should result in 404 error.
        String badUri = "/admin/retailer/111";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Id of the retailer is automatically set by the database (starting from 1)
        // Create retailer
        String uri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String inputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long id = createdRetailer.getId();

        // Create product for retailer
        String productUri = "/admin/retailer/" + id + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(50L);
        String productInputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(productUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(productInputJson)).andReturn();

        // Check if successful
        int productStatus = mvcResult.getResponse().getStatus();
        assertEquals(200, productStatus);
        String productContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(productContent, Product.class);
        Long productId = createdProduct.getId();

        // Get retailer by Id and check result.
        String idUri = "/admin/retailer/" + id;
        MvcResult idMvcResult = mvc.perform(MockMvcRequestBuilders.get(idUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = idMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = idMvcResult.getResponse().getContentAsString();
        Retailer receivedRetailer = super.mapFromJson(content, Retailer.class);
        assertEquals("06 12345678", receivedRetailer.getPhone());
        assertEquals(1, receivedRetailer.getProducts().size());
        assertEquals(createdProduct.getId(), receivedRetailer.getProducts().get(0).getId());
        assertEquals(createdProduct.getName(), receivedRetailer.getProducts().get(0).getName());
        assertEquals(createdProduct.getStock(), receivedRetailer.getProducts().get(0).getStock());

        // Delete product
        String productDeleteUri = "/admin/product/" + productId;
        MvcResult productDeleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(productDeleteUri)).andReturn();
        int productDeleteStatus = productDeleteMvcResult.getResponse().getStatus();
        assertEquals(204, productDeleteStatus);

        // Delete retailer.
        String deleteUri = "/admin/retailer/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void updateRetailer() throws Exception {
        // Create retailer.
        String uri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String inputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long id = createdRetailer.getId();

        // Non-existing id should result in 404 error.
        String badUri = "/admin/retailer/1000";
        Retailer badRetailer = new Retailer();
        badRetailer.setPhone("06 12345678");
        badRetailer.setProducts(Collections.emptyList());
        String badInputJson = super.mapToJson(badRetailer);
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.put(badUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(badInputJson)).andReturn();

        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Update retailer and check result.
        String updateUri = "/admin/retailer/" + id;
        Retailer updatedRetailer = new Retailer();
        updatedRetailer.setPhone("06 99999999");
        updatedRetailer.setProducts(Collections.emptyList());
        String updateInputJson = super.mapToJson(updatedRetailer);
        MvcResult updateMvcResult = mvc.perform(MockMvcRequestBuilders.put(updateUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updateInputJson)).andReturn();

        int status = updateMvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = updateMvcResult.getResponse().getContentAsString();
        Retailer receivedRetailer = super.mapFromJson(content, Retailer.class);
        assertEquals("06 99999999", receivedRetailer.getPhone());
        assertEquals(Collections.emptyList(), receivedRetailer.getProducts());

        // Delete retailer.
        String deleteUri = "/admin/retailer/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);
    }

    @Test
    public void deleteRetailer() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/admin/retailer/355";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.delete(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String inputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long id = createdRetailer.getId();

        // Delete retailer.
        String uri = "/admin/retailer/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);
    }

    @Test
    public void increaseProduct() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/admin/product/1000/inc/5";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();
        System.out.println(retailerId);

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(75L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createdProductContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(createdProductContent, Product.class);
        Long id = createdProduct.getId();

        // Negative amount should give 400 Bad Request error.
        String negUri = "/admin/product/" + id + "/inc/-5";
        MvcResult negMvcResult = mvc.perform(MockMvcRequestBuilders.get(negUri)).andReturn();
        int negStatus = negMvcResult.getResponse().getStatus();
        assertEquals(400, negStatus);

        // Increase stock of the product.
        String increaseUri = "/admin/product/" + id + "/inc/5";
        MvcResult increaseMvcResult = mvc.perform(MockMvcRequestBuilders.get(increaseUri)).andReturn();

        String increaseContent = increaseMvcResult.getResponse().getContentAsString();
        Product increasedProduct = super.mapFromJson(increaseContent, Product.class);
        assertEquals(id, increasedProduct.getId());
        assertEquals(Long.valueOf(80), increasedProduct.getStock());

        // Delete product.
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }

    @Test
    public void decrementProduct() throws Exception {
        // Non-existing id should give 404 error.
        String badUri = "/admin/product/1000/decr";
        MvcResult badMvcResult = mvc.perform(MockMvcRequestBuilders.get(badUri)).andReturn();
        int badStatus = badMvcResult.getResponse().getStatus();
        assertEquals(404, badStatus);

        // Create retailer.
        String postUri = "/admin/retailer";
        Retailer retailer = new Retailer();
        retailer.setPhone("06 12345678");
        retailer.setProducts(Collections.emptyList());
        String retInputJson = super.mapToJson(retailer);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(retInputJson)).andReturn();

        String createContent = res.getResponse().getContentAsString();
        Retailer createdRetailer = super.mapFromJson(createContent, Retailer.class);
        Long retailerId = createdRetailer.getId();

        // Create product for retailer
        String uri = "/admin/retailer/" + retailerId + "/product";
        Product product = new Product();
        product.setName("fries");
        product.setStock(1L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String createdProductContent = mvcResult.getResponse().getContentAsString();
        Product createdProduct = super.mapFromJson(createdProductContent, Product.class);
        Long id = createdProduct.getId();
        Long oldStock = createdProduct.getStock();

        // Decrement stock of the product.
        String increaseUri = "/admin/product/" + id + "/decr";
        MvcResult increaseMvcResult = mvc.perform(MockMvcRequestBuilders.get(increaseUri)).andReturn();

        String increaseContent = increaseMvcResult.getResponse().getContentAsString();
        Product increasedProduct = super.mapFromJson(increaseContent, Product.class);
        assertEquals(id, increasedProduct.getId());
        assertEquals(Long.valueOf(oldStock - 1), increasedProduct.getStock());

        // Decrementing a product not in stock anymore should result in a 400 Bad Request error.
        String negUri = "/admin/product/" + id + "/decr";
        MvcResult negMvcResult = mvc.perform(MockMvcRequestBuilders.get(negUri)).andReturn();
        int negStatus = negMvcResult.getResponse().getStatus();
        assertEquals(400, negStatus);

        // Delete product.
        String deleteUri = "/admin/product/" + id;
        MvcResult deleteMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteUri)).andReturn();
        int deleteStatus = deleteMvcResult.getResponse().getStatus();
        assertEquals(204, deleteStatus);

        // Delete retailer
        String deleteRetailerUri = "/admin/retailer/" + retailerId;
        MvcResult deleteRetailerMvcResult = mvc.perform(MockMvcRequestBuilders.delete(deleteRetailerUri)).andReturn();
        int deleteRetailerStatus = deleteRetailerMvcResult.getResponse().getStatus();
        assertEquals(204, deleteRetailerStatus);
    }
}
